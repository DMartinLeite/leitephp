#!/bin/sh

# Instal·lació MariaDB
apt-get update
apt-get install -y debconf-utils software-properties-common dirmngr
apt-key adv --recv-keys --no-tty --keyserver http://keyserver.ubuntu.com 0xF1656F24C74CD1D8
add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://tedeco.fi.upm.es/mirror/mariadb/repo/10.3/debian stretch main'
debconf-set-selections << EOF
mariadb-server-10.3 mysql-server/root_password password super3
mariadb-server-10.3 mysql-server/root_password_again password super3
EOF
apt-get update
apt-get install -y mariadb-server

# Instal·lació Apache i PHP
apt-get install -y apache2 libapache2-mod-php php-mysql
cp /vagrant/main-directory.conf /etc/apache2/conf-available
a2enconf main-directory
cp /vagrant/main-site.conf /etc/apache2/sites-available
a2dissite 000-default
a2ensite main-site
systemctl reload apache2

# Configuració PHP
sed -i s/"^display_errors .*"/"display_errors = On"/ /etc/php/7.0/apache2/php.ini
sed -i s/"^display_startup_errors .*"/"display_startup_errors = On"/ /etc/php/7.0/apache2/php.ini
sed -i s/"^error_reporting .*"/"error_reporting = E_ALL"/ /etc/php/7.0/apache2/php.ini
sed -i s/"^track_errors .*"/"track_errors = On"/ /etc/php/7.0/apache2/php.ini

# Creació base de dades
mysql -u root -psuper3 << EOF
CREATE DATABASE hotel;
USE hotel;
SOURCE /vagrant/hotel.sql
CREATE USER webuser@'localhost' IDENTIFIED BY 'super3';
GRANT ALL ON hotel.* TO webuser@'localhost';
EOF
systemctl restart mariadb
