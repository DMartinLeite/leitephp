<?php
require_once 'Connection.php';

// Exercici 2 - Informació dels preus per tipus d'habitació i temporada

// Completar

try {

  $conn = connect();
  $statement = $conn->prepare("SELECT Seasons.Name AS NameSea,RoomTypes.Name AS NameRoom,Price FROM RoomTypes JOIN PriceSeasons ON RoomTypes.Id = PriceSeasons.RoomTypeId JOIN Seasons ON PriceSeasons.SeasonId = Seasons.Id ORDER BY 1;");
  $statement->execute();
  //da los resultados n un array fetchall
  $prices = $statement->fetchAll();

}catch(PDOException $e) {
    echo "No s'ha pogut recuperar la llista de habitacions:\n{$e->getMessage()}\n";
    exit();
}

?>

<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Examen - exercici 2</title>
  </head>
  <body>
    <main role="main" class="container">
      <h1 class="mt-5">Examen - exercici 2</h1>
      <!-- Completar -->
      <table class='table table-striped'><tr><th>Season</th><th>Tipus HAB</th><th>Price</th></tr>
        <?php
        foreach ($prices as $price) {
          echo "<tr><td>{$price['NameSea']}</td><td>{$price['NameRoom']}</td><td>{$price['Price']}</td></tr>\n";
        }
        ?>
      </table>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
