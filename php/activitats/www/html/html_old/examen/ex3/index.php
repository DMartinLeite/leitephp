<?php
require_once 'Connection.php';

session_start();

// Exercici 3 - Inserció i esborrat de temporades.

// Completar

function show_messages() {
  if (isset($_SESSION['error'])) {
    echo "<div class='alert alert-danger' role='alert'>{$_SESSION['error']}</div>";
    unset($_SESSION['error']);
  }
  if (isset($_SESSION['success'])) {
    echo "<div class='alert alert-success' role='alert'>{$_SESSION['success']}</div>";
    unset($_SESSION['success']);
  }
}

try {

  $conn = connect();
  $statement = $conn->prepare("SELECT Name,StartingDay FROM Seasons");
  $statement->execute();
  //da los resultados n un array fetchall
  $desplegableseasons = $statement->fetchAll();

}catch(PDOException $e) {
    echo "No s'ha pogut recuperar la llista de seasons:\n{$e->getMessage()}\n";
    exit();
}

// PENSAMENT MISATGE SI NO HI HA CAP TEMPORADA
//try {

  //$con = connect();
//  $st = $con->prepare("SELECT COUNT(*) FROM Seasons");
//  $st->execute();
  //da los resultados n un array fetchall
//  $contarseasons = $statement->fetchAll();

//  if($contarseasons==0){
//    echo "NO HI HA CAP SEASON A LA BD";
//  }else{
//    echo "BENVINGUT A LAPOSTROFEXERCICI 3";
//  }

//}catch(PDOException $f) {
//    echo "No s'ha pogut recuperar la llista de seasons:\n{$f->getMessage()}\n";
//    exit();
//}

?>

<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Examen - exercici 3</title>
  </head>
  <body>
    <main role="main" class="container">
      <h1 class="mt-5">Examen - exercici 3</h1>
      <?php
      show_messages();
      ?>
      <h2 class="mt-5">Part 1 - Inserir temporades</h2>
      <!-- Completar -->
      <form action="insert.php" method="post">
        <div class="form-group">
          <label for="nom">Posa nom de la temporada</label>
          <input type="text" class="form-control" name="nom" id="nom" placeholder="temporada">
        </div>
        <div class="form-group">
          <label for="cognom">Posa la data</label>
          <input type="text" class="form-control" name="data" id="data" placeholder="Escriu la data">
        </div>
        <button type='submit' class='btn btn-primary'>Insereix</button>
          </form>
      <h2 class="mt-5">Part 2 - Esborrar temporades</h2>
      <!-- Completar -->

      <form class='form-inline' action='delete.php' method='post'>
        <select name="desplegableseasons">
        <div class="form-group">
          <?php
          foreach($desplegableseasons as $desplegablesea){
            echo "<option value={$desplegablesea["Name"]}> {$desplegablesea["Name"]} {$desplegablesea["StartingDay"]}</option>";
          }
          ?>

        </div>
        </select>
        <button type='submit' class='btn btn-primary'>DELETE</button>
      </form>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
