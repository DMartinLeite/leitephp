<?php
require_once 'Connection.php';

session_start();

// Completar

try {
  if ((!isset($_POST['nom'])) or (!isset($_POST['data']))){
    throw new Exception('Falten paràmetres.');
  }

  $nom = trim($_POST['nom']);
  $data = trim($_POST['data']);

    $conn = connect();
    $statement = $conn->prepare("INSERT INTO Seasons (Name,StartingDay) VALUES (:nom,:data)");
    $statement->bindParam(':nom', $nom);
    $statement->bindParam(':data', $data);
    $statement->execute();
    
    if (isset($nom)) {
      $_SESSION['success']=$nom ;
    }

    header('Location: index.php');
  exit();



} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}

?>
