<?php
require_once 'Connection.php';

session_start();

// Completar
try {
  if (!isset($_POST['desplegableseasons'])) {
    throw new Exception("Falten paràmetres.");
  }

  $desplegablese = trim($_POST['desplegableseasons']);


  $conn = connect();
  $statement = $conn->prepare("DELETE FROM Seasons Where Name LIKE :desplegablese");
  $statement->bindParam(':desplegablese', $desplegablese);
  $statement->execute();

  if (isset($desplegablese)) {
    $_SESSION['success']=$desplegablese ;
  }
  
  header('Location: index.php');
  exit();

} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}
?>
