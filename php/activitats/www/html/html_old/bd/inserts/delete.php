<?php
require_once 'Connection.php';

session_start();

try {
  if (!isset($_GET['id'])) {
    throw new Exception("Falten paràmetres.");
  }
  $id = trim($_GET['id']);
  $conn = connect();
  $statement = $conn->prepare("DELETE FROM Facilities WHERE Id = :id");
  $statement->bindParam(':id', $id);
  $statement->execute();
  header('Location: index.php');
  exit();
} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}

?>
